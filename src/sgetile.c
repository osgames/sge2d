/*
 * Copyright (c) 2007 Heiko Irrgang
 *
 * The license and distribution terms for this file may be
 * found in the file COPYING in this distribution or at
 * http://93-interactive.com/cms/products/software/sdl-game-engine/license/
 */

#include <sge.h>

SGETILEMAP *sgeTileMapNew(void) {
	SGETILEMAP *ret;
	sgeNew(ret, SGETILEMAP);
	ret->cameraX=0;
	ret->cameraY=0;
	ret->layers=sgeArrayNew();
	ret->layerTypes=sgeArrayNew();
	return ret;
}

void sgeTileMapDestroy(SGETILEMAP *m) {
	sgeFree(m);
}

SGETILELAYER *sgeTileLayerNew(int width, int height, int tileSize) {
	SGETILELAYER *ret;
	sgeNew(ret, SGETILELAYER);
	ret->width=width;
	ret->height=height;
	ret->tileSize=tileSize;
	sgeMalloc(ret->tiles, SGETILE*, width*height);
	return ret;
}

void sgeTileLayerDestroy(SGETILELAYER *l) {
	sgeFree(l);
}

SGETILE *sgeTileNew(SGESPRITE *s, int type, int x, int y, int isWalkable, int isJumpable) {
	SGETILE *ret;
	sgeNew(ret, SGETILE);
	ret->sprite=s;
	ret->type=type;
	ret->x=x;
	ret->y=y;
	ret->walkable=isWalkable;
	ret->jumpable=isJumpable;
	return ret;
}

void sgeTileDestroy(SGETILE *t) {
	sgeFree(t);
}

void sgeTileMapAdd(SGETILEMAP *m, void *layer, int type) {
    SGETILELAYERINFO *tli;
	sgeArrayAdd(m->layers, layer);
    sgeNew(tli, SGETILELAYERINFO);
    tli->type = type;
	sgeArrayAdd(m->layerTypes, tli);
}

SGETILELAYER *sgeTileMapGetLayer(SGETILEMAP *m, int layer) {
    return (SGETILELAYER *) sgeArrayGet( m->layers, layer );
}

int sgeTileMapGetLayerType(SGETILEMAP *m, int layer) {
    SGETILELAYERINFO *tli = (SGETILELAYERINFO *)sgeArrayGet( m->layerTypes, layer );
    return tli->type;
}

void sgeTileMapDraw(SGETILEMAP *m, SDL_Surface *dest) {
	int i;

	for ( i = 0; i < m->layers->numberOfElements; i++ ) {
		SGETILELAYER *l = (SGETILELAYER*) sgeArrayGet( m->layers, i );
		sgeTileLayerDraw(l, m, dest);
	}
}

void sgeTileMapSpriteToWorld( SGETILEMAP *m, SGEPOSITION *pos, int spriteX, int spriteY ) {
    pos->x = spriteX+m->cameraX;
    pos->y = spriteY+m->cameraY;
}

void sgeTileMapWorldToSprite( SGETILEMAP *m, SGEPOSITION *pos, int worldX, int worldY ) {
    pos->x = worldX-m->cameraX;
    pos->y = worldY-m->cameraY;
}

void sgeTileLayerAdd(SGETILELAYER *l, SGETILE *t) {
	l->tiles[t->y*l->width+t->x] = t;
}

void sgeTileLayerDraw(SGETILELAYER *l, SGETILEMAP *m, SDL_Surface *dest) {
	int x, y;
    SGETILE *t;
	// split up: tiles on screen: draw, tiles off screen update pos
	for (y=0;y<l->height;y++) {
		for (x=0;x<l->width;x++) {
			if (l->tiles[y*l->width+x]!=0) {
				t=l->tiles[y*l->width+x];
				t->sprite->x = (t->x*l->tileSize)-m->cameraX;
				t->sprite->y = (t->y*l->tileSize)-m->cameraY;
				sgeSpriteDraw(t->sprite, dest);
			}
		}
	}
}

SGETILE *sgeTileLayerTileFromCoords( SGETILELAYER *l, int worldX, int worldY ) {
    SGETILE *t;
    int rx = worldX/l->tileSize;
    int ry = worldY/l->tileSize;

    if ( ( rx >= 0 ) && ( rx < l->width ) ) {
        if ( ( ry >= 0 ) && ( ry < l->height ) ) {
            t=l->tiles[ry*l->width+rx];
            return t;

        }
    }
    return NULL;
}
