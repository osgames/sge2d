/*
 * Copyright (c) 2007 Heiko Irrgang
 *
 * The license and distribution terms for this file may be
 * found in the file COPYING in this distribution or at
 * http://agnosent.com/wolfcms/open-source/sge2d/license.html
 */
#include <sge.h>

int main(int argc, char **argv) {
    char *fname=NULL;
    char *uname=NULL;
    int fnameLength=0;
    char *hName=NULL;
    char *cppName=NULL;
    char *className=NULL;
    char *cname=NULL;
	struct stat st;
    FILE *f;

	if (argc!=2) {
        printf("sgewrapper - creates c++ wrappers for sge2d game states\n\n");
		printf("usage: sgewrapper ClassName\n");
		exit(0);
    }

    className=argv[1];
    fname=sgeLower(className);
    uname=sgeUpper(className);
    fnameLength=strlen(fname);

    sgeMalloc(hName, char, fnameLength+3);
    sprintf(hName, "%s.h", fname);
    sgeMalloc(cppName, char, fnameLength+5);
    sprintf(cppName, "%s.cpp", fname);

	if ( (stat(hName, &st)==0) || (stat(cppName, &st)==0) ) {
        printf("output files (%s or %s) exist\nPlease remove them if you are sure you do not need them anymore\n", hName, cppName);
        sgeFree(fname);
        sgeFree(uname);
        sgeFree(hName);
        sgeFree(cppName);
        exit(-1);
    }

    cname=strdup(className);
    cname[0]=tolower(cname[0]);

    printf("creating %s\n", hName);
    f=fopen(hName, "w");
    if (!f) {
        printf("ERROR: could not create %s\n", hName);
        sgeFree(fname);
        sgeFree(cname);
        sgeFree(uname);
        sgeFree(hName);
        sgeFree(cppName);
        exit(-1);
    }
    fprintf(f, "#ifndef __%s_H\n", uname);
    fprintf(f, "#define __%s_H\n\n", uname);
    fprintf(f, "#include <sge.h>\n\n");
    fprintf(f, "class %s {\n", className);
    fprintf(f, "\tprivate:\n");
    fprintf(f, "\t\tstatic %s* INSTANCE;\n", className);
    fprintf(f, "\t\t%s();\n\n", className);
    fprintf(f, "\tpublic:\n");
    fprintf(f, "\t\tstatic %s* singleton();\n", className);
    fprintf(f, "\t\tvoid onRedraw(SGEGAMESTATE *state);\n");
    fprintf(f, "\t\tint onKeyDown(SGEGAMESTATE *state, SGEEVENT *event);\n");
    fprintf(f, "\t\tint onKeyUp(SGEGAMESTATE *state, SGEEVENT *event);\n");
    fprintf(f, "\t\tvoid onMouseDown(SGEGAMESTATE *state, SGEEVENT *event);\n");
    fprintf(f, "\t\tvoid onMouseUp(SGEGAMESTATE *state, SGEEVENT *event);\n");
    fprintf(f, "\t\tvoid onMouseMove(SGEGAMESTATE *state, SGEEVENT *event);\n");
    fprintf(f, "};\n\n");
    fprintf(f, "extern \"C\" void %sOnRedraw(SGEGAMESTATE *state);\n", cname);
    fprintf(f, "extern \"C\" int %sOnKeyDown(SGEGAMESTATE *state, SGEEVENT *event);\n", cname);
    fprintf(f, "extern \"C\" int %sOnKeyUp(SGEGAMESTATE *state, SGEEVENT *event);\n", cname);
    fprintf(f, "extern \"C\" void %sOnMouseDown(SGEGAMESTATE *state, SGEEVENT *event);\n", cname);
    fprintf(f, "extern \"C\" void %sOnMouseUp(SGEGAMESTATE *state, SGEEVENT *event);\n", cname);
    fprintf(f, "extern \"C\" void %sOnMouseMove(SGEGAMESTATE *state, SGEEVENT *event);\n", cname);
    fprintf(f, "\nextern \"C\" void %sInit(SGEGAMESTATE *state);\n", cname);
    fprintf(f, "\n#endif\n");
    fclose(f);

    printf("creating %s\n", cppName);
    f=fopen(cppName, "w");
    if (!f) {
        printf("ERROR: could not create %s\n", cppName);
        sgeFree(fname);
        sgeFree(cname);
        sgeFree(uname);
        sgeFree(hName);
        sgeFree(cppName);
        exit(-1);
    }
    fprintf(f, "#include <%s>\n\n", hName);
    fprintf(f, "extern \"C\" void %sInit(SGEGAMESTATE *state) {\n", cname);
    fprintf(f, "\tstate->onRedraw=%sOnRedraw;\n", cname);
    fprintf(f, "\tstate->onKeyDown=%sOnKeyDown;\n", cname);
    fprintf(f, "\tstate->onKeyUp=%sOnKeyUp;\n", cname);
    fprintf(f, "\tstate->onMouseDown=%sOnMouseDown;\n", cname);
    fprintf(f, "\tstate->onMouseUp=%sOnMouseUp;\n", cname);
    fprintf(f, "\tstate->onMouseMove=%sOnMouseMove;\n", cname);
    fprintf(f, "}\n\n");
    fprintf(f, "extern \"C\" void %sOnRedraw(SGEGAMESTATE *state) {\n\treturn %s::singleton()->onRedraw(state);\n}\n\n", cname, className);
    fprintf(f, "extern \"C\" int %sOnKeyDown(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn %s::singleton()->onKeyDown(state, event);\n}\n\n", cname, className);
    fprintf(f, "extern \"C\" int %sOnKeyUp(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn %s::singleton()->onKeyUp(state, event);\n}\n\n", cname, className);
    fprintf(f, "extern \"C\" void %sOnMouseDown(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn %s::singleton()->onMouseDown(state, event);\n}\n\n", cname, className);
    fprintf(f, "extern \"C\" void %sOnMouseUp(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn %s::singleton()->onMouseDown(state, event);\n}\n\n", cname, className);
    fprintf(f, "extern \"C\" void %sOnMouseMove(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn %s::singleton()->onMouseMove(state, event);\n}\n\n", cname, className);
    fprintf(f, "\n%s *%s::INSTANCE=NULL;\n\n", className, className);
    fprintf(f, "%s *%s::singleton() {\n", className, className);
    fprintf(f, "\tif (!INSTANCE) {\n");
    fprintf(f, "\t\tINSTANCE=new %s();\n", className);
    fprintf(f, "\t}\n\treturn INSTANCE;\n}\n\n");
    fprintf(f, "%s::%s() {\n", className, className);
    fprintf(f, "\t/* constructor */\n");
    fprintf(f, "}\n\n");
    fprintf(f, "void %s::onRedraw(SGEGAMESTATE *state) {\n}\n\n", className);
    fprintf(f, "int %s::onKeyDown(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn NO;\n}\n\n", className);
    fprintf(f, "int %s::onKeyUp(SGEGAMESTATE *state, SGEEVENT *event) {\n\treturn NO;\n}\n\n", className);
    fprintf(f, "void %s::onMouseDown(SGEGAMESTATE *state, SGEEVENT *event) {\n}\n\n", className);
    fprintf(f, "void %s::onMouseUp(SGEGAMESTATE *state, SGEEVENT *event) {\n}\n\n", className);
    fprintf(f, "void %s::onMouseMove(SGEGAMESTATE *state, SGEEVENT *event) {\n}\n\n", className);
    fclose(f);

    sgeFree(fname);
    sgeFree(cname);
    sgeFree(uname);
    sgeFree(hName);
    sgeFree(cppName);
    printf("done\n");
    return 0;
}
