/**
 *
 * This is a demonstration of sge fade effects
 *
 * this is a easy way to fade between two bitmaps over a given time and
 * a given effect.
 *
 * look at sgefadefx.h for all effect types.
 *
 * the source, the destination and the target bitmap (default screen)
 * must have the same dimensions.
 *
 * WARNING:
 *
 * by default the sdl surfaces are not freed on sgeFadeFXDestroy(), you
 * have to explicitly query this by using
 *
 * sgeFadeFXDeleteSurfaces(fx, YES);
 * or sgeFadeFXDeleteSprites(fx, YES); (which is just an alias)
 *
 * */

#include <sge.h>

// define our data that is passed to our redraw function
typedef struct {
	SGESPRITE *src;
	SGESPRITE *dst;
	SGEFADEFX *fx;
	int showfx;
} MainStateData;

// redraw the screen and update game logics, if any
void on_redraw(SGEGAMESTATE *state)
{
	// prepare event and data variable form the gamestat passed to that
	// function
	SGEEVENTSTATE es = state->manager->event_state;
	MainStateData *data = (MainStateData*)state->data;
	SGESPRITE *nextsrc, *nextdst;

	// has the user closed the window?
	if (es.start.released) {
		sgeGameStateManagerQuit(state->manager);
		return;
	}

	// we do not need to clear the screen, as the whole screen is redrawn
	// by default

	if (!sgeFadeFXFinished(data->fx)) {
		sgeFadeFXDraw(data->fx);
	} else {
		// free effects data, you'll have to do this by hand, and we'll create a new one soon
		sgeFadeFXDestroy(data->fx);

		// switch between the effects
		data->showfx++;

		// change source and destination graphics for the new effect so its always alternating
		if (data->showfx%2==0) {
			nextsrc=data->src;
			nextdst=data->dst;
		} else {
			nextsrc=data->dst;
			nextdst=data->src;
		}

		switch (data->showfx%9) {
			case 1:
				// wipe left to right
				data->fx=sgeFadeFXNew(SGEFADEFX_WIPE_LEFT, 2000, nextsrc, nextdst);
				break;
			case 2:
				// wipe top to bottom
				data->fx=sgeFadeFXNew(SGEFADEFX_WIPE_TOP, 2000, nextsrc, nextdst);
				break;
			case 3:
				// wipe bottom to top
				data->fx=sgeFadeFXNew(SGEFADEFX_WIPE_BOTTOM, 2000, nextsrc, nextdst);
				break;
			case 4:
				// fade in
				data->fx=sgeFadeFXNew(SGEFADEFX_FADE, 2000, nextsrc, nextdst);
				break;
			case 5:
				// scroll left to right
				data->fx=sgeFadeFXNew(SGEFADEFX_SCROLL_LEFT, 2000, nextsrc, nextdst);
				break;
			case 6:
				// scroll right to left
				data->fx=sgeFadeFXNew(SGEFADEFX_SCROLL_RIGHT, 2000, nextsrc, nextdst);
				break;
			case 7:
				// scroll top to bottom
				data->fx=sgeFadeFXNew(SGEFADEFX_SCROLL_TOP, 2000, nextsrc, nextdst);
				break;
			case 8:
				// scroll bottom to top
				data->fx=sgeFadeFXNew(SGEFADEFX_SCROLL_BOTTOM, 2000, nextsrc, nextdst);
				break;
			default:
				// wipe right to left
				data->fx=sgeFadeFXNew(SGEFADEFX_WIPE_RIGHT, 2000, nextsrc, nextdst);
		}

	}

	// finally display the screen
	sgeFlip();
}

// this is the main function, you don't use main(), as this is handled different
// on some platforms
int run(int argc, char *argv[]) {
	SGEGAMESTATEMANAGER *manager;
	SGEGAMESTATE *mainstate;
	MainStateData data;
	SGEFILE *file;

	// initialize engine and set up resolution and depth
	sgeInit(NOAUDIO,NOJOYSTICK);
	sgeOpenScreen("SGE fade effects",320,240,32,NOFULLSCREEN);
	sgeHideMouse();

	// add a new gamestate. you will usually have to add different gamestates
	// like 'main menu', 'game loop', 'load screen', etc.
	mainstate = sgeGameStateNew();
	mainstate->onRedraw = on_redraw;
	mainstate->data = &data;

	// now finally create the gamestate manager and change to the only state
	// we defined, which is the on_redraw function
	manager = sgeGameStateManagerNew();
	sgeGameStateManagerChange(manager, mainstate);

	// load the two sprites to fade betweed, you can alternativly use SDL_Surfaces
	file=sgeOpenFile("data.d","asdf");
	data.src=sgeSpriteNewFile(file, "image1.png");
	data.dst=sgeSpriteNewFile(file, "image2.png");
	sgeCloseFile(file);

	// create our first effect, a wipe from right to left, taking 2000 ms
	data.fx=sgeFadeFXNew(SGEFADEFX_WIPE_RIGHT, 2000, data.src, data.dst);

	// just for demonstration on our fist effect we wait 1000ms before starting and
	// 2000 after finish
	sgeFadeFXPreDelay(data.fx, 1000);
	sgeFadeFXPostDelay(data.fx, 2000);
	data.showfx=0;

	// start the game running with 30 frames per seconds
	sgeGameStateManagerRun(manager, 30);

	// close the screen and quit
	sgeCloseScreen();
	return 0;
}
