/**
 *
 * This is a demonstration of sge's font effects
 *
 * sgeFontFXNew takes the following parameters:
 *
 * sgeFontFXNew(SGEFONT *font, Uint32 fxtype, Uint8 movement, Uint32 runtime, Uint32 charOffset, int startx, int starty, int endx, int endy, const char *text);
 *
 * font ->	a sge2d font
 *
 * fxtype ->	the effect type, these will change over time, have a look at sgefont.h for full list
 *
 * movement ->	determines how the text is moving between start and endpoint
 * 		can be:
 * 		SGEFONTFX_MOVE_LINEAR,
 * 		SGEFONTFX_MOVE_SLOWDOWN,
 * 		SGEFONTFX_MOVE_ACCELERATE
 *
 * runtime ->	miliseconds how long the animation takes... counts as per char (see charOffset)
 *
 * charOffset ->	miliseconds of delay between the single chars, use 0 to move the whole text at once
 *
 * startx,starty,endx,endy ->	the coordinates between the font is moving
 *
 * text ->	the text
 *
 * */

#include <sge.h>

// define our data that is passed to our redraw function
typedef struct {
	SGEFONT *font;
	SGEFONTFX *fx;
	int showfx;
} MainStateData;

// redraw the screen and update game logics, if any
void on_redraw(SGEGAMESTATE *state)
{
	// prepare event and data variable form the gamestat passed to that
	// function
	SGEEVENTSTATE es = state->manager->event_state;
	MainStateData *data = (MainStateData*)state->data;

	// has the user closed the window?
	if (es.start.released) {
		sgeGameStateManagerQuit(state->manager);
		return;
	}

	sgeClearScreen();
	if (!sgeFontFXFinished(data->fx)) {
		sgeFontFXDraw(data->fx);
	} else {
		// free effects data, you'll have to do this by hand, and we'll create a new one soon
		sgeFontFXDestroy(data->fx);

		// switch between the effects
		data->showfx=(data->showfx+1)%9;
		switch (data->showfx) {
			case 1:
				// fade font in, from top left (left outside the screen (-100) ), to the bottom of the screen
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_IN, SGEFONTFX_MOVE_SLOWDOWN, 1000, 100, -100, 0, 0, 220, "Hello World, look at my effects");
				sgeFontFXPostDelay(data->fx, 1000); // wait 1000ms after effect finished
				break;
			case 2:
				// fade font out from top of the screen
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_OUT, SGEFONTFX_MOVE_ACCELERATE, 1000, 100, 0, 0, 0, 220, "Hello World, look at my effects");
				sgeFontFXPreDelay(data->fx, 1000); // start effect after 1000ms
				break;
			case 3:
				// fade font in, this time we decrease the distance between the chars and use acceleration mode
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_IN, SGEFONTFX_MOVE_ACCELERATE, 1000, 20, 100, 0, 0, 220, "Hello World, look at my effects");
				sgeFontFXPostDelay(data->fx, 1000); // wait 1000ms after effect finished
				break;
			case 4:
				// fade font out, from top screen to bottom... dont process every single char
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_OUT, SGEFONTFX_MOVE_ACCELERATE, 1000, 0, 0, 0, 0, 240, "Hello World, look at my effects");
				break;
			case 5:
				// just fade in and out, no movement
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_INOUT, SGEFONTFX_MOVE_SLOWDOWN, 1000, 25, 0, 110, 0, 110, "Hello World, look at my effects");
				break;
			case 6:
				// fade font in, bouncing
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_IN, SGEFONTFX_MOVE_BOUNCE, 1500, 15, 0, 0, 0, 220, "Hello World, look at my effects");
				sgeFontFXPostDelay(data->fx, 1000); // wait 1000ms after effect finished
				break;
			case 7:
				// count from 5 to 0 in seconds (charoffset and text not in use)
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_COUNTDOWN, SGEFONTFX_MOVE_LINEAR, 5000, 0, 150, 100, 150, 100, "");
				sgeFontFXCountdownSetValues(data->fx,5,0);
				sgeFontFXPostDelay(data->fx, 1000); // wait 1000ms after effect finished
				break;
			case 8:
				// count from 0 to 654321 (charoffset and text not in use)
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_COUNTDOWN, SGEFONTFX_MOVE_SLOWDOWN, 3000, 0, 130, 100, 130, 100, "");
				sgeFontFXCountdownSetValues(data->fx,0,654321);
				sgeFontFXPreDelay(data->fx, 2000); // start effect after 2000ms
				sgeFontFXHideOnPreDelay(data->fx, YES); // hide until predelay is over, this way the effect is not drawn for 2000ms
				sgeFontFXPostDelay(data->fx, 1000); // wait 1000ms after effect finished
				break;
			default:
				// fade in from left top outside screen, to x 0 (-100 to 100, stopping in the middle), then fade out to x 100
				data->fx=sgeFontFXNew(data->font, SGEFONTFX_FADE_INOUT, SGEFONTFX_MOVE_SLOWDOWN, 1000, 100, -100, 20, 100, 220, "Hello World, look at my effects");
				break;
		}
	}

	// finally display the screen
	sgeFlip();
}

// this is the main function, you don't use main(), as this is handled different
// on some platforms
int run(int argc, char *argv[]) {
	SGEGAMESTATEMANAGER *manager;
	SGEGAMESTATE *mainstate;
	SGEFILE *f;
	MainStateData data;

	// initialize engine and set up resolution and depth
	sgeInit(NOAUDIO,NOJOYSTICK);
	sgeOpenScreen("SGE font effects",320,240,32,NOFULLSCREEN);
	sgeHideMouse();

	// add a new gamestate. you will usually have to add different gamestates
	// like 'main menu', 'game loop', 'load screen', etc.
	mainstate = sgeGameStateNew();
	mainstate->onRedraw = on_redraw;
	mainstate->data = &data;

	// load the bitmap font from the data file
	f=sgeOpenFile("data.d","asdf");
	data.font=sgeFontNewFile(f, SGEFONT_BITMAP, "font.png");
	sgeCloseFile(f);

	// fade in from left top outside screen, to x 0 (-100 to 100, stopping in the middle), then fade out to x 100
	data.fx=sgeFontFXNew(data.font, SGEFONTFX_FADE_INOUT, SGEFONTFX_MOVE_SLOWDOWN, 1000, 100, -100, 20, 100, 220, "Hello World, look at my effects");
	data.showfx=0;

	// now finally create the gamestate manager and change to the only state
	// we defined, which is the on_redraw function
	manager = sgeGameStateManagerNew();
	sgeGameStateManagerChange(manager, mainstate);

	// start the game running with 30 frames per seconds
	sgeGameStateManagerRun(manager, 30);

	sgeFontFXDestroy(data.fx);
	// close the screen and quit
	sgeCloseScreen();
	return 0;
}
