/**
 *
 * This is a demonstration of the event system.
 *
 * There is also a easy method for gpx or other consoles which allow you
 * to quickly query the common keys.
 *
 * If you need a more complex setup, this demo shows how to use event
 * handlers.
 *
 * The handlers are passed the current state and a SGEEVENT struct, which
 * is actually a SDL_Event struct, so read the SDL docs for more detail
 *
 * */

#include <sge.h>

// define our data that is passed to our redraw function
typedef struct {
	SGEFONT *font;
	int spaceDown;
	int quit;
	int leftMouse;
	int rightMouse;
	int mouseX;
	int mouseY;
} MainStateData;

// redraw the screen and update game logics, if any
void on_redraw(SGEGAMESTATE *state) {
	MainStateData *data = (MainStateData*)state->data;
	char buf[256];

	// check if escape has been pressed
	if (data->quit) {
		sgeGameStateManagerQuit(state->manager);
		return;
	}

	sgeClearScreen();
	sgeFontPrintBitmap(data->font, screen, 5, 5, "Press ESC to quit");

	// check if space is currently down
	if (data->spaceDown) {
		sgeFontPrintBitmap(data->font, screen, 5, 35, "Space is pressed");
	} else {
		sgeFontPrintBitmap(data->font, screen, 5, 35, "Space is NOT pressed");
	}
	// check left mouse button
	if (data->leftMouse) {
		sgeFontPrintBitmap(data->font, screen, 5, 55, "Left mouse button is down");
	} else {
		sgeFontPrintBitmap(data->font, screen, 5, 55, "Left mouse button is up");
	}
	// check right mouse button
	if (data->rightMouse) {
		sgeFontPrintBitmap(data->font, screen, 5, 75, "Right mouse button is down");
	} else {
		sgeFontPrintBitmap(data->font, screen, 5, 75, "Right mouse button is up");
	}

	// print mouse position
	if (data->mouseX>0) {
		snprintf(buf, 255, "Mouse x position: %d", data->mouseX);
		sgeFontPrintBitmap(data->font, screen, 5, 95, buf);
		snprintf(buf, 255, "Mouse y position: %d", data->mouseY);
		sgeFontPrintBitmap(data->font, screen, 5, 115, buf);
	}

	// finally display the screen
	sgeFlip();
}

// handle key down events
int on_key_down(SGEGAMESTATE *state, SGEEVENT *event) {
	if (event->key.keysym.sym==SDLK_SPACE) {
		MainStateData *data = (MainStateData*)state->data;
		data->spaceDown=1;
		return EVENT_HANDLED;
	}
	return EVENT_UNHANDLED;
}

// handle key up events
int on_key_up(SGEGAMESTATE *state, SGEEVENT *event) {
	MainStateData *data = (MainStateData*)state->data;
	switch (event->key.keysym.sym) {
		case SDLK_ESCAPE:
			data->quit=1;
			return EVENT_HANDLED;
		case SDLK_SPACE:
			data->spaceDown=0;
			return EVENT_HANDLED;
		default:
			// use default to suppress compiler warnings about unhandled keys
			break;
	}
	return EVENT_UNHANDLED;
}

// handle mouse down events
void on_mouse_down(SGEGAMESTATE *state, SGEEVENT *event) {
	MainStateData *data = (MainStateData*)state->data;
	switch (event->button.button) {
		case MOUSE_LEFT:
			data->leftMouse=1;
			break;
		case MOUSE_RIGHT:
			data->rightMouse=1;
			break;
	}
}

// handle mouse up events
void on_mouse_up(SGEGAMESTATE *state, SGEEVENT *event) {
	MainStateData *data = (MainStateData*)state->data;
	switch (event->button.button) {
		case MOUSE_LEFT:
			data->leftMouse=0;
			break;
		case MOUSE_RIGHT:
			data->rightMouse=0;
			break;
	}
}

// handle mouse move events
void on_mouse_move(SGEGAMESTATE *state, SGEEVENT *event) {
	MainStateData *data = (MainStateData*)state->data;
	data->mouseX=event->motion.x;
	data->mouseY=event->motion.y;
}

// this is the main function, you don't use main(), as this is handled different
// on some platforms
int run(int argc, char *argv[]) {
	SGEGAMESTATEMANAGER *manager;
	SGEGAMESTATE *mainstate;
	MainStateData data;
	SGEFILE *f;

	// initialize engine and set up resolution and depth
	sgeInit(NOAUDIO,NOJOYSTICK);
	sgeOpenScreen("Event demo",320,240,32,NOFULLSCREEN);

	// load the bitmap font from the data file
	f=sgeOpenFile("../font/data.d","asdf");
	data.font=sgeFontNewFile(f, SGEFONT_BITMAP, "font.png");
	sgeCloseFile(f);

	// initialize default values
	data.spaceDown=0;
	data.quit=0;
	data.leftMouse=0;
	data.rightMouse=0;
	data.mouseX=-1;
	data.mouseY=-1;

	// create the game state and specify the event handlers
	mainstate = sgeGameStateNew();
	mainstate->onRedraw = on_redraw;
	mainstate->onKeyDown = on_key_down;
	mainstate->onKeyUp = on_key_up;
	mainstate->onMouseDown = on_mouse_down;
	mainstate->onMouseUp = on_mouse_up;
	mainstate->onMouseMove = on_mouse_move;
	mainstate->data = &data;

	// now finally create the gamestate manager and change to the only state
	// we defined, which is the on_redraw function
	manager = sgeGameStateManagerNew();
	sgeGameStateManagerChange(manager, mainstate);

	// start the game running with 30 frames per seconds
	sgeGameStateManagerRun(manager, 30);

	// close the screen and quit
	sgeCloseScreen();
	return 0;
}
