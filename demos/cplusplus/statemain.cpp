/*
 * use the sgestate tool to create a wrapper for your
 * states, in this case you would have used
 *
 * tools/sgestate StateMain
 */
#include <statemain.h>

/*
 * declare our C function to call our cpp instance.
 *
 * this is what we use as game state in the state manager.
 */
extern "C" void stateMain(SGEGAMESTATE *state) {
	StateMain *m=StateMain::singleton();
	m->draw(state);
}

StateMain *StateMain::INSTANCE=NULL;

/*
 * load our resources in the class constructor, as this class
 * is set up as singleton, you might want to get a singleton
 * before running your game to and show a load screen while
 * your resourced get loaded
 */
StateMain::StateMain() {
	spriteRotation=.0f;

	SGEFILE *file;
	file=sgeOpenFile("data.d","asdf");
	this->sprite=sgeSpriteNewFile(file, "sprite.png");
	sgeCloseFile(file);

	this->sprite->x=136;
	this->sprite->y=88;
}

StateMain *StateMain::singleton() {
	if (!INSTANCE) {
		INSTANCE=new StateMain();
	}
	return INSTANCE;
}

// now finally our draw method
void StateMain::draw(SGEGAMESTATE *state) {
	// get the event state
	SGEEVENTSTATE es = state->manager->event_state;

	// exit if return / start is pressed
	if (es.start.released) {
		sgeGameStateManagerQuit(state->manager);
		return;
	}

	sgeClearScreen();

	// draw a rotating sprite
	sgeSpriteDrawRotoZoomed(this->sprite, this->spriteRotation, 1.0f, screen);

	// flip the screen
	sgeFlip();

	this->spriteRotation+=0.1f;
}
