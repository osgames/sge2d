/*
 * use the sgestate tool to create a wrapper for your
 * states, in this case you would have used
 *
 * tools/sgestate StateMain
 */
#ifndef _STATEMAIN_H
#define _STATEMAIN_H

#include <sge.h>

/*
 * define a singleton class that handles the game draing in the
 * draw() method
 */
class StateMain {
	private:
		static StateMain* INSTANCE;
		StateMain();
		SGESPRITE *sprite;
		float spriteRotation;

	public:
		static StateMain* singleton();
		void draw(SGEGAMESTATE *state);
};

/*
 * define a C function that is added to the sge stage system
 * which then calls our c class
 */
extern "C" void stateMain(SGEGAMESTATE *state);

#endif
