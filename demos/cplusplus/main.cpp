/*
 * use the sgestate tool to create a wrapper for your
 * states, in this case you would have used
 *
 * tools/sgestate StateMain
 */
#include <statemain.h>

extern "C" int run(int argc, char *argv[]) {
	// initialize screen
	sgeInit(NOAUDIO,NOJOYSTICK);
	sgeOpenScreen("sge2d with c++ demo",320,240,32,NOFULLSCREEN);
	sgeHideMouse();

	SGEGAMESTATEMANAGER *manager;
	SGEGAMESTATE *mainstate;

	/*
	 * create the main state and set the C (!) function stateMain
	 * (from statemain.h/cpp) as onRedraw
	 */
	mainstate = sgeGameStateNew();
	mainstate->onRedraw = stateMain;

	// add the state to the state manager
	manager = sgeGameStateManagerNew();
	sgeGameStateManagerChange(manager, mainstate);

	// run it with 30 fps
	sgeGameStateManagerRun(manager, 30);

	sgeCloseScreen();
	return 0;
}
